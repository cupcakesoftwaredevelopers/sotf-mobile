package nl.cupcake.shopofthefuture.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by emmap on 20-1-2016.
 */
public class Utils {

    public static void changeIconColor(ImageView icon, int newColor) {
        icon.getDrawable().mutate().setColorFilter(newColor, PorterDuff.Mode.MULTIPLY);
    }

    public static boolean isEmpty(String s) {
        return s == null || s.length() == 0;
    }

    public static boolean jsonHasAll(JSONObject json, String[] attributes) {
        for (int i=0; i<attributes.length; i++) {
            if (!json.has(attributes[i])) return false;
        }
        return true;
    }

    public static String formatPrice(double price){
        DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
        symbols.setDecimalSeparator(',');
        symbols.setGroupingSeparator('.');

        NumberFormat formatter = new DecimalFormat("#,##0.00", symbols);

        return formatter.format(price);
    }

    public static String formatDate(long date){
        try {
            Date d = new Date(date);
            SimpleDateFormat df = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
            return df.format(d);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public static void hideSoftKeyboard(Activity activity) {
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null) {
            InputMethodManager manager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(currentFocus.getApplicationWindowToken(), 0);
        }
    }
}
