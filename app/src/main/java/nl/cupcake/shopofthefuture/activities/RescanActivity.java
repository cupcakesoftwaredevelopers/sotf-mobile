package nl.cupcake.shopofthefuture.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import nl.cupcake.shopofthefuture.R;
import nl.cupcake.shopofthefuture.adapters.RescanAdapter;
import nl.cupcake.shopofthefuture.model.Product;
import nl.cupcake.shopofthefuture.preferences.PreferenceManager;

/**
 * Created by Stefan on 28-1-2016.
 */
public class RescanActivity extends AppCompatActivity {

    private static final int MIN_RESCAN = 3;

    private ArrayList<Product> products = new ArrayList<>();
    private ArrayList<Product> productsToScan = new ArrayList<>();
    private PreferenceManager preferenceManager;
    private RescanAdapter adapter;
    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rescan);

        Bundle bundle = getIntent().getExtras();
        products = (ArrayList<Product>) bundle.getSerializable("products");
        preferenceManager = PreferenceManager.getInstance(this);

        initToolbar();
        initButton();
        initList();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        toolbar.setNavigationIcon(R.drawable.ic_credit_card);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.rescan_title));
    }

    private void initButton() {
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startScan();
            }
        });
    }

    private void initList() {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rescan_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        initProductsToScan();

        adapter = new RescanAdapter(productsToScan);
        recyclerView.setAdapter(adapter);
    }

    private void initProductsToScan() {
        if (products.size() > MIN_RESCAN) {
            Random random = new Random();
            int randomScanAmount = getRandomScanAmount(products.size());

            HashMap<Long, Product> randomProducts = new HashMap<>();

            while (randomProducts.size() != randomScanAmount) {
                Product product = products.get(random.nextInt(products.size()));
                if (randomProducts.containsKey(product.getBarcode())) {
                    try {
                        randomProducts.put(product.getBarcode(), new Product(product.getJSON()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            productsToScan.addAll(randomProducts.values());
        } else {
            for (Product p : products) {
                try {
                    productsToScan.add(new Product(p.getJSON()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public void startScan() {
        new IntentIntegrator(RescanActivity.this).initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result.getContents() == null) {
            Log.d("RescanActivity", "Cancelled scan");
        } else {
            Log.d("RescanActivity", "Scanned: " + result.getContents());
            String code = result.getContents();
            if (code != null) {
                try {
                    long parsed = Long.valueOf(code);
                    handleScan(parsed);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void handleScan(final long code) {
        boolean allScanned = true;
        boolean found = false;

        for (Product product : productsToScan) {
            if (code == product.getBarcode()) {
                product.setScanned(true);
                found = true;
            }
            if (!product.isScanned()) {
                allScanned = false;
            }
        }

        adapter.notifyDataSetChanged();
        if (allScanned) {
            preferenceManager.setRescan(false);
            preferenceManager.setWasScanned(true);
            Bundle bundle = new Bundle();
            bundle.putSerializable("products", new ArrayList<>(products));
            Intent intent = new Intent(RescanActivity.this, TransactionActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }

        if (!found) {
            Snackbar.make(fab, R.string.unknown_product, Snackbar.LENGTH_LONG).show();
        }
    }

    private int getRandomScanAmount(int totalProducts) {
        if (totalProducts > MIN_RESCAN) {
            Random random = new Random();
            return MIN_RESCAN + random.nextInt(totalProducts - MIN_RESCAN);
        } else {
            return totalProducts;
        }
    }
}
