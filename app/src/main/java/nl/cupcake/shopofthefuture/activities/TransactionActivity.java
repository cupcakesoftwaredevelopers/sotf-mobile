package nl.cupcake.shopofthefuture.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import nl.cupcake.shopofthefuture.R;
import nl.cupcake.shopofthefuture.api.API;
import nl.cupcake.shopofthefuture.model.Product;
import nl.cupcake.shopofthefuture.preferences.PreferenceManager;
import nl.cupcake.shopofthefuture.utils.Utils;

/**
 * Created by Stefan on 26-1-2016.
 */
public class TransactionActivity extends AppCompatActivity {

    private API api;
    private PreferenceManager preferenceManager;
    private ArrayList<Product> products;
    private int basketID = -1;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        Bundle bundle = getIntent().getExtras();
        products = (ArrayList<Product>) bundle.getSerializable("products");

        api = API.getInstance(this);
        preferenceManager = PreferenceManager.getInstance(this);

        initToolbar();
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        JSONObject basket = new JSONObject();
        JSONArray array = new JSONArray();
        double total = 0D;
        for (Product product : products) {
            total += (product.getAmount() * product.getPrice());
            try {
                array.put(product.getJSON());
            } catch (JSONException e) {
            }
        }

        try {
            basket.put("products", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView priceView = (TextView) findViewById(R.id.transaction_price);
        priceView.setText(Utils.formatPrice(total));

        showLoader(true);

        final double finalTotal = total;
        API.getInstance(this).sendBasket(basket, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONObject json = null;
                try {
                    json = API.getInstance(TransactionActivity.this).getAuthenticationJson();
                    basketID = response.getInt("basket_id");
                    json.put("basket_id", basketID);
                    json.put("price", finalTotal);
                    showLoader(false);
                    ImageView qr = (ImageView) findViewById(R.id.transaction_code);
                    qr.setImageBitmap(createQR(json));
                } catch (JSONException e) {
                    e.printStackTrace();
                    showLoader(false);
                    Snackbar.make(findViewById(R.id.transaction_code), R.string.no_qr_generated, Snackbar.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showLoader(false);
                Snackbar.make(findViewById(R.id.transaction_code), R.string.no_qr_generated, Snackbar.LENGTH_LONG).show();
            }
        });

    }

    private void showLoader(boolean show) {
        if (show) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        toolbar.setNavigationIcon(R.drawable.ic_credit_card);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.transaction_title));
    }

    private Bitmap createQR(JSONObject json) {
        String text = json.toString();

        QRCodeWriter qr = new QRCodeWriter();
        int size = getQrDimension();
        BitMatrix qrCode = null;
        try {
            qrCode = qr.encode(text, BarcodeFormat.QR_CODE, size, size);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        int w = qrCode.getWidth();
        int h = qrCode.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = qrCode.get(x, y) ? Color.BLACK : Color.TRANSPARENT;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, size, 0, 0, w, h);

        return bitmap;
    }

    private int getQrDimension() {
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int dimension = point.x < point.y ? point.x : point.y;
        return dimension * 3 / 4;
    }

    public void acceptTransaction(final View v) {
        final Activity activity = this;

        API api = API.getInstance(getApplicationContext());
        api.checkPayment(basketID, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    boolean paid = response.getBoolean("paid");
                    if (paid) {
                        Intent intent = new Intent(getApplicationContext(), ShoppingListActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra(ShoppingListActivity.SHOULD_BE_CLEARED, true);
                        activity.startActivity(intent);
                        activity.finish();
                    } else {
                        Snackbar.make(v, R.string.not_paid, Snackbar.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Snackbar.make(v, R.string.not_paid, Snackbar.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Snackbar.make(v, R.string.no_connection, Snackbar.LENGTH_LONG).show();
            }
        });


    }

    public void cancelTransaction(View v) {
        onBackPressed();
    }
}
