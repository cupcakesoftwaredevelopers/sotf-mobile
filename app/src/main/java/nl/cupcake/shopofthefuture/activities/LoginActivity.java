package nl.cupcake.shopofthefuture.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import nl.cupcake.shopofthefuture.R;
import nl.cupcake.shopofthefuture.api.API;
import nl.cupcake.shopofthefuture.preferences.PreferenceManager;
import nl.cupcake.shopofthefuture.utils.Utils;

/**
 * Created by Stefan on 21-1-2016.
 */
public class LoginActivity extends AppCompatActivity {

    private API api;
    private PreferenceManager preferenceManager;

    private EditText username;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        api = API.getInstance(this);
        preferenceManager = PreferenceManager.getInstance(this);

        if (!Utils.isEmpty(preferenceManager.getToken()) && preferenceManager.getUser() != null) {
            startActivity(new Intent(LoginActivity.this, ShoppingListActivity.class));
            finish();
        }

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);

        initToolbar();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_key_variant);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.login_title));
    }

    public void login(final View v) {
        Utils.hideSoftKeyboard(this);
        v.setEnabled(false);
        String username = this.username.getText().toString().trim().toLowerCase();
        String password = this.password.getText().toString();
        if (Utils.isEmpty(username) || Utils.isEmpty(password)) {
            Snackbar.make(v, getString(R.string.login_error_none), Snackbar.LENGTH_LONG).show();
            v.setEnabled(true);
        } else {
            api.doLogin(username, password, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        preferenceManager.setToken(response.getString(PreferenceManager.TOKEN));
                        preferenceManager.setUser(response.getInt(PreferenceManager.USER));
                        startActivity(new Intent(LoginActivity.this, ShoppingListActivity.class));
                        finish();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        v.setEnabled(true);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("LoginActivity", error.toString());
                    Snackbar.make(v, getString(R.string.login_error), Snackbar.LENGTH_LONG).show();
                    v.setEnabled(true);
                }
            });
        }
    }
}

