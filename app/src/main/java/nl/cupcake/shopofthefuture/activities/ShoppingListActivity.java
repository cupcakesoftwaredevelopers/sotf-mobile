package nl.cupcake.shopofthefuture.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import nl.cupcake.shopofthefuture.R;
import nl.cupcake.shopofthefuture.adapters.ShoppingListAdapter;
import nl.cupcake.shopofthefuture.api.API;
import nl.cupcake.shopofthefuture.exceptions.InvalidJsonException;
import nl.cupcake.shopofthefuture.model.Product;
import nl.cupcake.shopofthefuture.preferences.PreferenceManager;
import nl.cupcake.shopofthefuture.utils.Utils;

/**
 * Created by Stefan on 21-1-2016.
 */
public class ShoppingListActivity extends AppCompatActivity {

    private static final int RANDOM = 10; // chances: 1=0-0=100%, 10=0-9=10%, 100=0-99=1%
    public static final String SHOULD_BE_CLEARED = "should_be_cleared";

    private API api;
    private PreferenceManager preferenceManager;
    private ArrayList<Product> products = new ArrayList<>();

    //private Map<String, Product> notScanned = new HashMap<>();
    private Map<Long, Product> scanned = new HashMap<>();
    private Map<Long, Product> shoppingList = new HashMap<>();

    private ShoppingListAdapter adapter;
    private RecyclerView recyclerView;
    private RelativeLayout relativeLayout;
    private FloatingActionButton fab;
    private TextView totalTextView;
    private ImageView payButton;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);

        // Init below
        api = API.getInstance(this);
        preferenceManager = PreferenceManager.getInstance(this);

        totalTextView = (TextView) findViewById(R.id.shopping_list_total_value);
        payButton = (ImageView) findViewById(R.id.confirm_shopping_cart);

        initToolbar();
        initButton();
        initList();

        // Checks below
        checkClear(getIntent());
        checkPayButton();
    }

    @Override
    protected void onDestroy() {
        saveBasket();
        super.onDestroy();
    }

    private void checkClear(Intent data) {
        Intent intent = getIntent();
        if (intent.hasExtra(SHOULD_BE_CLEARED)) {
            boolean clear = intent.getBooleanExtra(SHOULD_BE_CLEARED, false);
            if (clear) {
                preferenceManager.setWasScanned(false);
                preferenceManager.clearBasket();
                scanned.clear();
                updateViews();
            }
        }
    }

    private void saveBasket() {
        JSONArray array = new JSONArray();
        try {
            for (long key : scanned.keySet()) {
                array.put(scanned.get(key).getJSON());
            }
            preferenceManager.setBasket(array);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadBasket() {
        JSONArray array = preferenceManager.getBasket();
        if (array != null) {
            try {
                for (int i = 0; i < array.length(); i++) {
                    Product product = new Product(array.getJSONObject(i));
                    product.setScanned(true);
                    scanned.put(product.getBarcode(), product);
                    products.add(product);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, getString(R.string.load_basket_failed), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        toolbar.setNavigationIcon(R.drawable.ic_checkbox_marked_circle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.shopping_list_title));

        ImageView imageView = (ImageView) findViewById(R.id.toolbar_bonhistorie);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ShoppingListActivity.this, HistoryActivity.class));
            }
        });
        imageView.setVisibility(View.VISIBLE);
    }

    private void initButton() {
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startScan();
            }
        });
    }

    private void initList() {
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getShoppingList();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.shopping_list);
        relativeLayout = (RelativeLayout) findViewById(R.id.no_list_container);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ShoppingListAdapter(products, this);
        recyclerView.setAdapter(adapter);
        getShoppingList();
    }

    private void showLoadingScreen(Boolean show) {
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        if (show) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    public void showInfo(boolean show) {
        if (show) {
            relativeLayout.setVisibility(View.VISIBLE);
        } else {
            relativeLayout.setVisibility(View.GONE);
        }
    }

    private void getShoppingList() {
        showLoadingScreen(true);
        loadBasket();
        updateTotalPrice();

        api.getShoppingList(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    products.clear();
                    products.addAll(scanned.values());
                    JSONArray products = response.getJSONArray("products");
                    for (int i = 0; i < products.length(); i++) {
                        Product product = new Product(products.getJSONObject(i));
                        shoppingList.put(product.getBarcode(), product);
                        if (!scanned.containsKey(product.getBarcode())) {
                            ShoppingListActivity.this.products.add(product);
                        }
                    }

                    updateViews();

                    mSwipeRefreshLayout.setRefreshing(false);
                    showLoadingScreen(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("ShoppingListActivity", error.toString());
                Snackbar.make(recyclerView, R.string.no_connection, Snackbar.LENGTH_INDEFINITE).show();
                showLoadingScreen(false);
            }
        });
    }

    public void updateTotalPrice() {
        double price = 0D;
        for (Product product : products) {
            if (product.isScanned()) {
                price += product.getPrice() * product.getAmount();
            }
        }
        totalTextView.setText(Utils.formatPrice(price));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        checkClear(data);

        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result.getContents() == null) {
            Log.d("ShoppingListActivity", "Cancelled scan");
        } else {
            Log.d("ShoppingListActivity", "Scanned: " + result.getContents());
            String code = result.getContents();
            if (code != null)
                try {
                    long parsed = Long.valueOf(code);
                    handleScan(parsed);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
        }
    }

    private void handleScan(final long code) {
        if (scanned.containsKey(code)) {
            Product product = scanned.get(code);
            product.increaseAmount();
            product.setScanned(true);
            updateViews(true);
        } else if (shoppingList.containsKey(code)) {
            Product product = shoppingList.get(code);
            product.setScanned(true);
            scanned.put(code, product);
            if (product.getAmount() <= 0) product.setAmount(1);
            updateViews(true);
        } else {
            api.doRequest("product/" + code, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Product product = new Product(response);
                        product.setScanned(true);
                        products.add(product);
                        scanned.put(code, product);
                        updateViews(true);
                    } catch (InvalidJsonException e) {
                        e.printStackTrace();
                        Snackbar.make(ShoppingListActivity.this.fab, getString(R.string.unknown_product), Snackbar.LENGTH_LONG).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("ShoppingListActivity", error.toString());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(ShoppingListActivity.this.fab, getString(R.string.unknown_product), Snackbar.LENGTH_LONG).show();
                        }
                    });
                }
            });
        }
    }

    private void checkInfo() {
        if (products.size() == 0) showInfo(true);
        else showInfo(false);
    }

    private void checkPayButton() {
        if (scanned.size() > 0) {
            Utils.changeIconColor(payButton, ContextCompat.getColor(this, R.color.primary));
            payButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!preferenceManager.getWasScanned()) {
                        Random random = new Random();
                        if (random.nextInt(RANDOM) == 0) {
                            preferenceManager.setRescan(true);
                        }
                    }
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("products", new ArrayList<>(scanned.values()));
                    Intent intent;
                    if (preferenceManager.getRescan() && !preferenceManager.getWasScanned()) {
                        intent = new Intent(ShoppingListActivity.this, RescanActivity.class);
                    } else {
                        intent = new Intent(ShoppingListActivity.this, TransactionActivity.class);
                    }
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        } else {
            Utils.changeIconColor(payButton, ContextCompat.getColor(this, R.color.accent));
            payButton.setOnClickListener(null);
        }
    }

    private void deleteProduct(Product product) {
        products.remove(product);
        updateViews();
    }

    public void startScan() {
        new IntentIntegrator(ShoppingListActivity.this).initiateScan();
    }

    private void updateViews(boolean added) {
        if (preferenceManager.getWasScanned() && added) { // was update in cart
            preferenceManager.setWasScanned(false);
        }
        updateViews();
    }

    public void updateViews() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                checkInfo();
                checkPayButton();
                adapter.notifyDataSetChanged();
                updateTotalPrice();
            }
        });
    }

    public void unScanProduct(Product product) {
        if (scanned.containsKey(product.getBarcode())) {
            scanned.remove(product.getBarcode());
            product.setScanned(false);
            if (!shoppingList.containsKey(product.getBarcode())) {
                deleteProduct(product);
            }
        }
        updateViews();
    }

    public void fullRemoveProduct(final Product product) {
        if (shoppingList.containsKey(product.getBarcode())) {
            api.removeFromShoppingList(product, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        unScanProduct(product);

                        Log.d("response", response.toString());
                        if (response.getString("status").equals("success")) {
                            if (shoppingList.containsKey(product.getBarcode()))
                                shoppingList.remove(product.getBarcode());
                            if (products.contains(product))
                                products.remove(product);
                        } else {
                            Snackbar.make(findViewById(R.id.footer), R.string.failed_to_remove, Snackbar.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        Snackbar.make(findViewById(R.id.footer), R.string.failed_to_remove, Snackbar.LENGTH_SHORT).show();
                        e.printStackTrace();
                    } finally {
                        updateViews();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
        } else {
            unScanProduct(product);
        }
    }
}
