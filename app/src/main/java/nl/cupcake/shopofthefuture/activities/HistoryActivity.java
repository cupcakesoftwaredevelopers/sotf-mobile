package nl.cupcake.shopofthefuture.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import nl.cupcake.shopofthefuture.R;
import nl.cupcake.shopofthefuture.adapters.HistoryAdapter;
import nl.cupcake.shopofthefuture.api.API;
import nl.cupcake.shopofthefuture.model.Basket;
import nl.cupcake.shopofthefuture.preferences.PreferenceManager;

/**
 * Created by Stefan on 26-1-2016.
 */
public class HistoryActivity extends AppCompatActivity {

    private API api;
    private PreferenceManager preferenceManager;
    private ArrayList<Basket> history = new ArrayList<>();
    private HistoryAdapter adapter;
    private TextView textView;
    private RecyclerView recyclerView;
    private LinearLayout progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        api = API.getInstance(this);
        preferenceManager = PreferenceManager.getInstance(this);

        progressBar = (LinearLayout) findViewById(R.id.progress_bar_wrapper);
        progressBar.setVisibility(View.VISIBLE);

        initToolbar();
        initList();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_history);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.history_title));
    }

    private void initList() {
        getHistoryList();
        recyclerView = (RecyclerView) findViewById(R.id.history_list);
        textView = (TextView) findViewById(R.id.no_history_text);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new HistoryAdapter(history);
        recyclerView.setAdapter(adapter);
    }

    private void getHistoryList() {
        api.getHistoryList(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray baskets = response.getJSONArray("history");
                    for (int i = 0; i < baskets.length(); i++) {
                        history.add(new Basket(baskets.getJSONObject(i)));
                    }

                    progressBar.setVisibility(View.GONE);
                    updateView();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("HistoryActivity", error.toString());
            }
        });
    }

    private void updateView(){
        if ( ! history.isEmpty()) {
            textView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        adapter.notifyDataSetChanged();
    }

}
