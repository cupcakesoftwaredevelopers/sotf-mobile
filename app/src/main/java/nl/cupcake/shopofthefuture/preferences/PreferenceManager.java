package nl.cupcake.shopofthefuture.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;

/**
 * Created by Stefan on 22-1-2016.
 */
public class PreferenceManager {

    public static final String TOKEN = "token";
    public static final String USER = "user";
    public static final String RESCAN = "rescan";
    public static final String WAS_SCANNED = "scanned";
    public static final String BASKET = "basket";


    private static PreferenceManager preferenceManager;
    private static SharedPreferences sharedPreferences;

    private PreferenceManager(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

    public static PreferenceManager getInstance(Context context) {
        if (preferenceManager == null) {
            preferenceManager = new PreferenceManager(context);
        }
        return preferenceManager;
    }

    public String getToken() {
        return sharedPreferences.getString(TOKEN, null);
    }

    public Integer getUser() {
        int id = sharedPreferences.getInt(USER, -1);
        return (id == -1) ? null : id;
    }

    public void setToken(String token) {
        sharedPreferences.edit().putString(TOKEN, token).apply();
    }

    public void setUser(Integer user) {
        sharedPreferences.edit().putInt(USER, user).apply();
    }

    public void setBasket(JSONArray json) {
        sharedPreferences.edit().putString(BASKET, json.toString()).apply();
    }

    public boolean getRescan() {
        return sharedPreferences.getBoolean(RESCAN, false);
    }

    public JSONArray getBasket() {
        try {
            String basketString = sharedPreferences.getString(BASKET, null);
            if (basketString == null) return null;
            return new JSONArray(basketString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setRescan(boolean rescan) {
        sharedPreferences.edit().putBoolean(RESCAN, rescan).apply();
    }

    public void clearBasket() {
        sharedPreferences.edit().putString(BASKET, null).apply();
    }

    public boolean getWasScanned() {
        return sharedPreferences.getBoolean(WAS_SCANNED, false);
    }

    public void setWasScanned(boolean scanned) {
        sharedPreferences.edit().putBoolean(WAS_SCANNED, scanned).apply();
    }
}
