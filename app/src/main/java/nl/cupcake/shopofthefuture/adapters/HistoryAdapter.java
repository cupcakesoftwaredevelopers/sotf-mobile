package nl.cupcake.shopofthefuture.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import nl.cupcake.shopofthefuture.R;
import nl.cupcake.shopofthefuture.model.Basket;

/**
 * Created by Stefan on 26-1-2016.
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private ArrayList<Basket> history;

    public HistoryAdapter(ArrayList<Basket> history) {
        this.history = history;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_history_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Basket basket = this.history.get(position);
        holder.date.setText(basket.getDateString());
        holder.price.setText(basket.getTotalString());
    }

    @Override
    public int getItemCount() {
        return history.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView date;
        public TextView price;

        public ViewHolder(View v) {
            super(v);
            date = (TextView) v.findViewById(R.id.history_date);
            price = (TextView) v.findViewById(R.id.history_price);
        }
    }
}
