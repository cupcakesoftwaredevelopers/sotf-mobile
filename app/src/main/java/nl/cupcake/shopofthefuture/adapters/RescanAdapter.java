package nl.cupcake.shopofthefuture.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import nl.cupcake.shopofthefuture.R;
import nl.cupcake.shopofthefuture.model.Product;

/**
 * Created by Stefan on 28-1-2016.
 */
public class RescanAdapter extends RecyclerView.Adapter<RescanAdapter.ViewHolder> {

    private ArrayList<Product> products;

    public RescanAdapter(ArrayList<Product> products) {
        this.products = products;
    }

    @Override
    public RescanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_rescan_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RescanAdapter.ViewHolder holder, int position) {
        Product product = products.get(position);
        holder.scanned.setChecked(product.isScanned());
        holder.info.setText(StringUtils.capitalize(product.getBrand() + " " + product.getName()));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public CheckBox scanned;
        public TextView info;

        public ViewHolder(View v) {
            super(v);
            scanned = (CheckBox) v.findViewById(R.id.scanned);
            info = (TextView) v.findViewById(R.id.rescan_info);
        }
    }
}
