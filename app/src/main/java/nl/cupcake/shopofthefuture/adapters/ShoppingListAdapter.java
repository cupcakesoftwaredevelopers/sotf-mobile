package nl.cupcake.shopofthefuture.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

import nl.cupcake.shopofthefuture.R;
import nl.cupcake.shopofthefuture.activities.ShoppingListActivity;
import nl.cupcake.shopofthefuture.model.Product;
import nl.cupcake.shopofthefuture.utils.Utils;

/**
 * Created by Stefan on 25-1-2016.
 */
public class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListAdapter.ViewHolder> {

    private ArrayList<Product> products;
    private ShoppingListActivity shoppingListActivity;

    public ShoppingListAdapter(ArrayList<Product> products, ShoppingListActivity shoppingListActivity) {
        this.products = products;
        this.shoppingListActivity = shoppingListActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_shopping_list, parent, false);
        Utils.changeIconColor((ImageView) v.findViewById(R.id.shopping_list_plus), ContextCompat.getColor(parent.getContext(), R.color.accent));
        Utils.changeIconColor((ImageView) v.findViewById(R.id.shopping_list_minus), ContextCompat.getColor(parent.getContext(), R.color.accent));
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (position + 1 == getItemCount()) {
            holder.checkBox.setVisibility(View.INVISIBLE);
            holder.name.setVisibility(View.INVISIBLE);
            holder.detail.setVisibility(View.INVISIBLE);
            holder.price.setVisibility(View.INVISIBLE);
            holder.amount.setVisibility(View.INVISIBLE);
            holder.plus.setVisibility(View.INVISIBLE);
            holder.minus.setVisibility(View.INVISIBLE);
            holder.currency.setVisibility(View.INVISIBLE);
        } else {
            final Product product = products.get(position);
            if (product.isScanned()) {
                holder.checkBox.setChecked(true);
            }
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.name.setVisibility(View.VISIBLE);
            holder.detail.setVisibility(View.VISIBLE);
            holder.price.setVisibility(View.VISIBLE);
            holder.amount.setVisibility(View.VISIBLE);
            holder.plus.setVisibility(View.VISIBLE);
            holder.minus.setVisibility(View.VISIBLE);
            holder.currency.setVisibility(View.VISIBLE);

            holder.name.setText(StringUtils.capitalize(product.getBrand() + " " + product.getName()));
            holder.detail.setText(product.getDetail());
            holder.price.setText(Utils.formatPrice(product.getPrice()));
            holder.amount.setText(String.valueOf(product.getAmount()));
            holder.checkBox.setChecked(product.isScanned());

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.checkBox.setChecked(false);
                    if (product.isScanned()) {
                        shoppingListActivity.unScanProduct(product);
                    } else {
                        shoppingListActivity.startScan();
                    }
                }
            });

            holder.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    product.increaseAmount();
                    notifyDataSetChanged();
                    shoppingListActivity.updateTotalPrice();
                }
            });

            holder.minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    product.decreaseAmount();
                    if (product.getAmount() <= 0) {
                        holder.checkBox.setChecked(false);
                        removeProduct(product);
                    }
                    shoppingListActivity.updateViews();
                }
            });

            holder.rootLayout.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    createDeleteDialog(product);
                    return true;
                }
            });
        }
    }

    private void removeProduct(Product product) {
        shoppingListActivity.unScanProduct(product);
    }

    @Override
    public int getItemCount() {
        return products.size() + 1;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public RelativeLayout rootLayout;
        public CheckBox checkBox;
        public TextView name;
        public TextView detail;
        public TextView price;
        public TextView amount;
        public ImageView plus;
        public ImageView minus;
        public TextView currency;

        public ViewHolder(View v) {
            super(v);
            rootLayout = (RelativeLayout) v.findViewById(R.id.shopping_list_item_root_layout);
            checkBox = (CheckBox) v.findViewById(R.id.got_this);
            name = (TextView) v.findViewById(R.id.shopping_list_product_name);
            detail = (TextView) v.findViewById(R.id.shopping_list_product_detail);
            price = (TextView) v.findViewById(R.id.shopping_list_product_price);
            amount = (TextView) v.findViewById(R.id.shopping_list_product_amount);
            plus = (ImageView) v.findViewById(R.id.shopping_list_plus);
            minus = (ImageView) v.findViewById(R.id.shopping_list_minus);
            currency = (TextView) v.findViewById(R.id.shopping_list_currency_sign);
        }
    }

    private void createDeleteDialog(final Product product) {
        AlertDialog.Builder builder = new AlertDialog.Builder(shoppingListActivity);
        builder.setMessage(R.string.ask_remove_product);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                shoppingListActivity.fullRemoveProduct(product);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        AlertDialog dialog = builder.create();

        dialog.show();
    }


}
