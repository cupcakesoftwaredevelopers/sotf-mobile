package nl.cupcake.shopofthefuture.exceptions;

import org.json.JSONException;

/**
 * Created by emmap on 22-1-2016.
 */
public class InvalidJsonException extends JSONException {

    public InvalidJsonException() {
        super("Wrong JSON for this object.");

    }

}
