package nl.cupcake.shopofthefuture.model;

import org.json.JSONException;
import org.json.JSONObject;

import nl.cupcake.shopofthefuture.exceptions.InvalidJsonException;
import nl.cupcake.shopofthefuture.utils.Utils;

/**
 * Created by Stefan on 26-1-2016.
 */
public class Basket {

    private int id;
    private String code;
    private Long date;
    private int paid;
    private double total;

    public Basket(JSONObject basket) throws InvalidJsonException {
        if (!Utils.jsonHasAll(basket, new String[]{"id","code", "date", "paid", "total"})) {
            throw new InvalidJsonException();
        }
        try {
            id = basket.getInt("id");
            code = basket.getString("code");
            date = basket.getLong("date");
            paid = basket.getInt("paid");
            total = basket.getDouble("total");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("id", id);
        json.put("code", code);
        json.put("date", date);
        json.put("paid", paid);
        json.put("total", total);
        return json;
    }

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public long getDate() {
        return date;
    }

    public String getDateString(){
        return Utils.formatDate(getDate());
    }

    public int getPaid() {
        return paid;
    }

    public double getTotal() {
        return total;
    }

    public String getTotalString() {
        return Utils.formatPrice(getTotal());
    }
}
