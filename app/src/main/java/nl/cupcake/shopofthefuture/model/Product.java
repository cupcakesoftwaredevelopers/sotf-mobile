package nl.cupcake.shopofthefuture.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import nl.cupcake.shopofthefuture.exceptions.InvalidJsonException;
import nl.cupcake.shopofthefuture.utils.Utils;

/**
 * Created by emmap on 22-1-2016.
 */
public class Product implements Serializable {

    private long barcode;
    private String name;
    private String brand;
    private String detail;
    private double price;
    private int amount;
    private boolean scanned = false;

    public Product(int barcode) {
        this.barcode = barcode;
    }

    public Product(JSONObject productJSON) throws InvalidJsonException {
        if (!Utils.jsonHasAll(productJSON, new String[]{"name", "brand", "detail", "price"})) {
            throw new InvalidJsonException();
        }
        try {
            barcode = productJSON.getLong("barcode");
            name = productJSON.getString("name");
            brand = productJSON.getString("brand");
            detail = productJSON.getString("detail");
            price = productJSON.getDouble("price");
            if (productJSON.has("amount")) {
                amount = productJSON.getInt("amount");
            } else {
                amount = 1;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("barcode", barcode);
        json.put("name", name);
        json.put("brand", brand);
        json.put("detail", detail);
        json.put("price", price);
        json.put("amount", amount);
        return json;
    }

    public long getBarcode() {
        return barcode;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public String getDetail() {
        return detail;
    }

    public double getPrice() {
        return price;
    }

    public int getAmount() {
        return amount;
    }

    public int increaseAmount() {
        amount++;
        return amount;
    }

    public int decreaseAmount() {
        amount--;
        if (amount < 0) {
            amount = 0;
        }
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isScanned() {
        return scanned;
    }

    public void setScanned(boolean scanned) {
        this.scanned = scanned;
    }
}
