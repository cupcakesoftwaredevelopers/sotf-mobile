package nl.cupcake.shopofthefuture.api;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import nl.cupcake.shopofthefuture.model.Product;
import nl.cupcake.shopofthefuture.preferences.PreferenceManager;

/**
 * Created by Stefan on 21-1-2016.
 */
public class API {

    private static final String SERVER_HOST = "http://143.176.18.91:8123/";
    //private static final String SERVER_HOST = "http://10.0.2.2:8080/"; //localhost emulator

    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";

    private static final String ROUTE_LOGIN = "login";
    private static final String ROUTE_SHOPPING_LIST = "shoppinglist";
    private static final String ROUTE_HISTORY = "history";
    private static final String ROUTE_BASKET = "basket";

    private static API api;
    private static RequestQueue queue;
    private static PreferenceManager prefs;


    private API(Context context) {
        if (queue == null) {
            queue = Volley.newRequestQueue(context);
            prefs = PreferenceManager.getInstance(context);
        }
    }

    public static API getInstance(Context context) {
        if (api == null) {
            api = new API(context);
        }
        return api;
    }

    private void addRequestToQueue(JsonObjectRequest request) {
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    public void doRequest(String route, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, SERVER_HOST + route, responseListener, errorListener);
        addRequestToQueue(request);
    }

    public void doRequest(String route, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener, JSONObject jsonObject) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, SERVER_HOST + route, jsonObject, responseListener, errorListener);
        addRequestToQueue(request);
    }

    public void doLogin(String username, String password, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(USERNAME, username);
        params.put(PASSWORD, password);
        JSONObject jsonObject = new JSONObject(params);
        doRequest(ROUTE_LOGIN, responseListener, errorListener, jsonObject);
    }

    public void getShoppingList(Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {
        doRequest(ROUTE_SHOPPING_LIST, responseListener, errorListener, getAuthenticationJson());
    }

    //TODO configure json object
    public void removeFromShoppingList(Product product, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {
        JSONObject json = getAuthenticationJson();
        try {
            json.put("barcode", product.getBarcode());
        } catch (Exception e) {}
        doRequest(ROUTE_SHOPPING_LIST + "/delete", responseListener, errorListener, json);
    }

    public void getHistoryList(Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener)  {
        doRequest(ROUTE_BASKET + "/" + ROUTE_HISTORY, responseListener, errorListener, getAuthenticationJson());
    }

    public JSONObject getAuthenticationJson()  {
        try {
            return new JSONObject().put(PreferenceManager.TOKEN, prefs.getToken()).put(PreferenceManager.USER, prefs.getUser());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new JSONObject(); // not authorised
    }

    public void checkPayment(int basketID, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {
        JSONObject json = getAuthenticationJson();
        try {
            json.put("basket_id", basketID);
        } catch (Exception e) { e.printStackTrace(); }
        doRequest(ROUTE_BASKET + "/check", responseListener, errorListener, json);
    }

    public void sendBasket(JSONObject basket, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {
        JSONObject json = getAuthenticationJson();
        try {
            json.put("basket", basket);
        } catch (JSONException e) {}
        doRequest(ROUTE_BASKET, responseListener, errorListener, json);
    }
}
